import twilio from 'twilio'

import config from '../../../config/app'
const client = twilio(config.get('twilio.accountSid'), config.get('twilio.authToken'));

const Messaging = (args) => {
    const {
        To,
        From,
        message
    } = args

    client.messages.create({
        from: '+16187151621',
        to: '+918296705464',
        body: "hi",
    })

    return {
        To,
        From
    }
}

export default Messaging